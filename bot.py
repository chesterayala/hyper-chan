# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

from botbuilder.core import ActivityHandler, TurnContext
from botbuilder.core.teams.teams_info import TeamsInfo
from botbuilder.core.teams.teams_activity_handler import TeamsActivityHandler
from botbuilder.schema.teams import TeamsChannelAccount
import configparser
import openai
import json
import subprocess

config = configparser.ConfigParser()
config.read('config.ini')

openai.api_key = config['OpenAI']['token']

servers = config['HyperV']['hostnames'].split(',')

def execute_command(command, error_only=False, wait=True):
    print(command)
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if wait:
        stdout, stderr = process.communicate()

        if error_only:
            return stderr

        return stdout

def get_machine_info(hostname, properties=None, specific_server=None):
    info = {}
    if specific_server:
        command = ['powershell.exe', f'Get-VM -ComputerName {specific_server} -Name {hostname}']
        if properties:
            command.append(f'| Select-Object -Property {properties}')
        command.append(f'| ConvertTo-Json')
        output = execute_command(command)
        info = json.loads(output)
    else:
        for server in servers:
            command = ['powershell.exe', f'Get-VM -ComputerName {server} -Name {hostname}']
            if properties:
                command.append(f'| Select-Object -Property {properties}')
            command.append(f'| ConvertTo-Json')
            output = execute_command(command)
            if output:
                info = json.loads(output)
                break
    return info

class OpenAI():
    def submit_query(message):
        with open('models/hyperv.txt', 'r') as f:
            model = f.read()
        
        response = openai.Completion.create(
                engine='text-davinci-002',
                prompt=f'{model}\n\nUser:{message}\nBot:',
                temperature=0,
                max_tokens=100,
                top_p=1,
                frequency_penalty=0.2,
                presence_penalty=0,
                best_of=1
        )

        if response['choices'][0]['text']:
            try:
                task = json.loads(response['choices'][0]['text'].lstrip())
                return task
            except Exception:
                return response['choices'][0]['text'].lstrip()

        else:
            return 'No response from OpenAI server.'

class MyBot(TeamsActivityHandler):
    async def on_message_activity(self, turn_context: TurnContext):
        message = turn_context.activity.text.replace('<at>Hyper-chan</at>' , '').replace('<at>Hyper-chan </at>' , '').lstrip().rstrip()
        print(f'User: {message}')


        ##################################################
        # LUIS Request code.
        ##################################################
        # response = requests.get(
        #     f'{config["LUIS"]["prediction_endpoint"]}luis/prediction/v3.0/apps/{config["LUIS"]["app_id"]}/slots/production/predict', 
        #     headers={}, 
        #     params={
        #         'query': message,
        #         'subscription-key': config["LUIS"]["prediction_key"]
        #     }
        # )
        # hostnames = response.json()['prediction']['entities']['hostname']
        # hostnames = list(set(hostnames))
        # intent = response.json()['prediction']['topIntent']
        #
        # if intent == 'Hyperv.Start':
        #     do start code

        response = OpenAI.submit_query(message)
        print(f'Bot: {response}')

        ##################################################
        # Fall back, in case OpenAI/LUIS isn't accessible.
        ##################################################
        # if message.startswith('start'):
        #     hostname = message.split(' ')[1]
        #     await turn_context.send_activity(f'Looking up for {hostname} from Hyper-V machines...')
        #     info = get_machine_info(hostname, 'Name, State, ComputerName')
        #     if info:
        #         if info['State'] == 2:
        #             await turn_context.send_activity(f'{hostname} is already turned on.')
        #         else:
        #             server = info['ComputerName']
        #             execute_command(command = ['powershell.exe', f'Start-VM -ComputerName {server} -Name {hostname}'], wait = False)
        #             await turn_context.send_activity(f'{hostname} is now turned on. Please wait for the OS to boot.')
        #     else:
        #         await turn_context.send_activity(f'{hostname} does not exists in any of these machines:\n {", ".join(servers)}')
        #     return
        
        if type(response) == dict:
            if response['task'] == 'start':
                for hostname in response['targets']:
                    await turn_context.send_activity(f'Searching for {hostname} from Hyper-V list.')
                    info = get_machine_info(hostname, 'Name, State, ComputerName')
                    if info:
                        if info['State'] == 2:
                            await turn_context.send_activity(f'{hostname} is already turned on.')
                        else:
                            server = info['ComputerName']
                            output = execute_command(command = ['powershell.exe', f'Start-VM -ComputerName {server} -Name {hostname}'], error_only = True, wait = True)
                            info = get_machine_info(hostname, 'State', server)
                            if info['State'] == 2:
                                await turn_context.send_activity(f'{hostname} is now turned on. Please wait for a couple of minutes for the OS to boot.')
                            else:
                                output = str(output, 'UTF-8')
                                output = output.split('\r\n')[1]
                                output += ' Please email it-s@openit.com for these concerns.'
                                await turn_context.send_activity(output)
                    else:
                        await turn_context.send_activity(f'{hostname} does not exists in any of these machines:\n {", ".join(servers)}')
            elif response['task'] == 'stop':
                hostnames = response.json()['prediction']['entities']['hostname']
                hostnames = list(set(hostnames))

                for hostname in response['targets']:
                    await turn_context.send_activity(f'Searching for {hostname} from Hyper-V list.')
                    info = get_machine_info(hostname, 'Name, State, ComputerName')
                    if info:
                        if info['State'] == 2:
                            await turn_context.send_activity(f'{hostname} is already turned off.')
                        else:
                            server = info['ComputerName']
                            execute_command(command = ['powershell.exe', f'Start-VM -ComputerName {server} -Name {hostname}'], wait = False)
                            await turn_context.send_activity(f'{hostname} is now turned off.')
                    else:
                        await turn_context.send_activity(f'{hostname} does not exists in any of these machines:\n {", ".join(servers)}')
        else:
            await turn_context.send_activity(response)

    async def on_teams_members_added(
        self,
        teams_members_added: TeamsChannelAccount,
        team_info: TeamsInfo,
        turn_context: TurnContext,
    ):
        for member in teams_members_added:
            if member.id != turn_context.activity.recipient.id:
                await turn_context.send_activity(f"Welcome {member.name} to {team_info.name}.")
